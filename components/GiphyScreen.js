import React, {useEffect, useState} from 'react';  
import { Button, Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";

 const GiphyScreen = props => {
    const [giphy, setGiphy] = useState("")
    const [fetch, setFetching] = useState(false)
    useEffect(() => {
        const fetchData  = async () => {
            const apiRoot = "https://api.giphy.com";
            const apiKey = process.env.REACT_APP_GIPHY_KEY;
            const result = await axios (`${apiRoot}/v1/gifs/random?api_key=${apiKey}`)
            console.log(result)
            setGiphy(result.data.data.images.fixed_height.url)
        };
        fetchData();
    }, [fetch])
    return (
        <div className="center">
            <Card style={{ width: '40rem' }}>
                <Card.Img variant="top" style={{height:"350px", widows:"100%"}} src={giphy}/>
                <Card.Body>
                    <Button variant="primary" onClick={() => setFetching(!fetch) }>Gimme Giphy</Button>
                </Card.Body>
            </Card>
        </div>
    );
}

export default GiphyScreen;
